'use strict';
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Fetches the local service registry assigned to a service id
 * @param {Object} serviceId - service id
 * @returns {Object} a local service registry
 */
function getService(serviceId) {

    // var callTestGet = LocalServiceRegistry.createService("test.http.get", {
    //     createRequest: function(svc: HTTPService, args) {
    //         svc.setRequestMethod("GET");
    //     },
    //     parseResponse: function(svc: HTTPService, client: HTTPClient) {
    //         return client.text;
    //     },
    //     mockCall: function(svc: HTTPService, client: HTTPClient) {
    //         return {
    //             statusCode: 200,
    //             statusMessage: "Success",
    //             text: "MOCK RESPONSE (" + svc.URL + ")"
    //         };
    //     },
    //     filterLogMessage: function(msg: String) {
    //         return msg.replace("headers", "OFFWITHTHEHEADERS");
    //     }
    // });

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {
            svc.addHeader("Content-type","application/json");
            svc.addHeader("Accept","application/json");
            var key = dw.system.Site.getCurrent().getCustomPreferenceValue("api-key");

            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                    svc.addParam("hapikey",key)
                //svc.addHeader("Content-Type", "text/json");
                return JSON.stringify(args);
            } else {
                return null;
            }
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        },
    });
}

module.exports = {
    getService: getService
};
