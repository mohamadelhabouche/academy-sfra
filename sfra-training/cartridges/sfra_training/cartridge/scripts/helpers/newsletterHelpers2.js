'use strict';
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Fetches the local service registry assigned to a service id
 * @param {Object} serviceId - service id
 * @returns {Object} a local service registry
 */
function getService(serviceId) {
    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {
            svc.setRequestMethod("GET");
            var key = dw.system.Site.getCurrent().getCustomPreferenceValue("api-key");
            svc.addParam("hapikey", key);
            if(args>0 && args){
                svc.addParam("limit", args)
            }
            else {
                svc.addParam("limit", 10)
            }
            return JSON.stringify(args);
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        },
    });
}

function getServiceDel(serviceId) {
    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {

            svc.setRequestMethod("DELETE");
            var key = dw.system.Site.getCurrent().getCustomPreferenceValue("api-key");
            svc.addParam("hapikey", key);

            return JSON.stringify(args);
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        },
    });
}

function getServiceUp(serviceId) {
    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {

            svc.setRequestMethod("PATCH");
            svc.addHeader("Content-type","application/json");
            var key = dw.system.Site.getCurrent().getCustomPreferenceValue("api-key");
            svc.addParam("hapikey", key);

            return JSON.stringify(args);
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        },
    });
}
module.exports = {
    getServiceUp: getServiceUp,
    getServiceDel: getServiceDel,
    getService: getService
    
};
