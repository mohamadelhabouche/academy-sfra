'use strict'

function prod(product) {


    var id = product.ID;
    var name = product.name;
    var description = product.shortDescription;

    return{
        ID : id,
        Name : name,
        Description : description
    };
}

exports.prod = prod;
/*

function prod(product) {


    var customObject = {};

    Transaction.wrap(function(){

        customObject = CustomObjectMgr.createCustomObject()
    });
}
*/