'use strict'

function deleted(data) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');

        var customObject = CustomObjectMgr.getCustomObject(data.customObject, data.id);

        Transaction.wrap(function(){
            CustomObjectMgr.remove(customObject);
        });
}

exports.deleted = deleted;