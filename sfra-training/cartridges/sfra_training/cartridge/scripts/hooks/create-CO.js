'use strict'

function create(data) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');

    var currentTimeStamp = StringUtils.formatCalendar(new Calendar(new Date()), "yyyyMMddHHmmssSS");
        
    Transaction.wrap(function(){
        var customObject = CustomObjectMgr.createCustomObject("Newsletter", currentTimeStamp);

        customObject.custom.nome = data.properties.firstname;
        customObject.custom.cognome = data.properties.lastname;
        customObject.custom.email = data.properties.email;
        customObject.custom.timestamp = currentTimeStamp;
    });
    return{
        success: true,
        timeStamp: currentTimeStamp
    };
}

exports.create = create;