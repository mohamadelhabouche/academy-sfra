'use strict';

var formValidation = require('base/components/formValidation');

function ClientsNb() {
    $('form.NewsForm').on('submit', function (e) {
        console.log('prova');
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('action');
        $form.spinner().start();
        $('form.NewsForm').trigger('newsletterList:submit', e);
        $.ajax({
            url: url,
            type: 'GET',
            data: $form.serialize(),
            success: function (data) {
                $form.spinner().stop();

                    console.log(data);
                    document.open();
                    document.write(data);
                    document.close();

                    //window.location.href = data.redirectUrl;
                    
                
            },
            error: function (err) {
                // if (err.responseJSON.redirectUrl) {
                //     window.location.href = err.responseJSON.redirectUrl;
                // }
                $form.spinner().stop();
            }
        });
        return false;
    });
}

function ClientDel() {
    $('form.DeleteForm').on('submit', function (e) {
        console.log('prova');
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('action');
        $form.spinner().start();
        $('form.DeleteForm').trigger('newsletterList:submit', e);
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                $form.spinner().stop();
                if (!data.success) {
                    console.log("ERROR");
                    //formValidation($form, data);
                } else {
                    var className='.' + data.Id;
                    //window.location.href = data.redirectUrl;
                    $(className).text(data.response);
                    $(className).removeClass('hidden');
                    console.log(className);
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }
                $form.spinner().stop();
            }
        });
        return false;
    });
}

module.exports = {
    ClientDel: ClientDel,
    ClientsNb: ClientsNb
    
};