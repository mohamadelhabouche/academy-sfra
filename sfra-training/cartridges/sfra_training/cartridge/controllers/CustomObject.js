'use strict'

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var SeekableIterator = require('dw/util/SeekableIterator');
var Site = require('dw/system/Site');

server.get(
    'Create',
    server.middleware.https,
    function (req, res, next) {
        var name = req.querystring.nome;
        var surname = req.querystring.cognome;
        var email = req.querystring.email;

        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        
        Transaction.wrap(function(){
            var customObject = CustomObjectMgr.createCustomObject("Newsletter", currentTimeStamp);

            customObject.custom.nome = name;
            customObject.custom.cognome = surname;
            customObject.custom.email = email;
            customObject.custom.timestamp = currentTimeStamp;

            var jasonResponse = {};
            jasonResponse.success = true;
            jasonResponse.timeStamp = currentTimeStamp;

            res.json(jasonResponse);
            next();
        });
    }
);

server.get(
    'Read',
    server.middleware.https,
    function (req, res, next) {        
            var customObjects = CustomObjectMgr.getAllCustomObjects("Newsletter");
            var jasonResponse = {};
            var list = [];

            

            while(customObjects.hasNext())
            {
                var objects = customObjects.next();
                var name = objects.custom.nome;
                var surname = objects.custom.cognome;
                var email = objects.custom.email;
                var id = objects.custom.timestamp;
                var jasonObject = {
                    "name" : name,
                    "surname" : surname,
                    "email" : email,
                    "id" : id
                }
                list.push(jasonObject);
            }

            jasonResponse.items = list
            jasonResponse.count = 10
            res.json(jasonResponse);
            next();
        
    }
);


server.get(
    'delete',
    server.middleware.https,
    function (req, res, next) {

        var id = req.querystring.id;
        var customObject = CustomObjectMgr.getCustomObject("Newsletter", id);

        Transaction.wrap(function(){
            CustomObjectMgr.remove(customObject);
        });
        var jasonResponse = {};
        jasonResponse.success = true;
        res.json(jasonResponse);
        next();
        
    }
);


module.exports =  server.exports();