'use strict'

var server = require('server');
var Site = require('dw/system/Site');
var ProductMgr = require('dw/catalog/ProductMgr');
var HookMgr = require('dw/system/HookMgr');
var Logger = require('dw/system/Logger');

server.get(
    'show',
    server.middleware.https,
    function (req, res, next) {
        
        var active = dw.system.Site.getCurrent().getCustomPreferenceValue("isAcademyProductActive");

        if(active) {

            var product = ProductMgr.getProduct('25591911M');
            var prodhook ={};
            prodhook = dw.system.HookMgr.callHook("dw.product","prod",product);
            res.render('academyProduct/academyProduct.isml', prodhook);
            next();
        }

        else {
            var customLog = Logger.getLogger('noIDLog','warn');
            customLog.warn('ID not found');
            res.render('academyProduct/academyProductError.isml');
            next();
        }
    }
);

server.get(
    'create',
    server.middleware.https,
    function (req, res, next) {
        
        var active = dw.system.Site.getCurrent().getCustomPreferenceValue("isAcademyProductActive");
        

        if(active) {

            var product = ProductMgr.getProduct('25591911M');

            var customhook = dw.system.HookMgr.callHook("dw.product","prod",product);
            
            
            res.render('academyProduct/academyProduct.isml', {
                product: product,
                status: "success"
            });
            next();
        }

        else {
            res.render('academyProduct/academyProductError.isml')
            next();
        }
    }
);



module.exports =  server.exports();