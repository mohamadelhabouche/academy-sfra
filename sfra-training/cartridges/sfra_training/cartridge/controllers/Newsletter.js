'use strict';

var server = require('server');
var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var URLUtils = require('dw/web/URLUtils');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var SeekableIterator = require('dw/util/SeekableIterator');
var HookMgr = require('dw/system/HookMgr');
var Logger = require('dw/system/Logger');
var customLog = Logger.getLogger('CrmServiceLogger','warn');


server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        //var actionUrl = dw.web.URLUtils.url('Newsletter-Handler');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Subscribe'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.render('/newsletter/newslettersuccess', {
                continueUrl: continueUrl,
                newsletterForm: newsletterForm
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.render('/newsletter/newslettererror', {
                errorMsg: dw.web.Resource.msg('error.crossfieldvalidation', 'newsletter', null),
                continueUrl: continueUrl
            });
        }

        next();
    }
);
server.get(
    'GetList',
    server.middleware.https,
    function (req, res, next) {
        var limit = req.querystring.ClientsNb;
        var newsletterHelpers2 = require('*/cartridge/scripts/helpers/newsletterHelpers2');
        var service = newsletterHelpers2.getService('get.list');
        var response = service.call(limit);
        if (response.ok) {
            var jsonResponse = []
            var object = JSON.parse(response.object);
            var results = object.results;
            
            for (var i=0; i < results.length; i++){
                var data = {};
                    data.firstname = results[i].properties.firstname;
                    data.lastname = results[i].properties.lastname;
                    data.email = results[i].properties.email;
                    data.id = results[i].id;
                
                jsonResponse.push(data);
            }

            res.render('newsletter/newsletterlist.isml', {
                success: true,
                response: jsonResponse
            });
            next();
        }
    }
);

server.get(
    'delete',
    server.middleware.https,
    function (req, res, next) {

        var id = req.querystring.ClientDel;
        var newsletterHelpers2 = require('*/cartridge/scripts/helpers/newsletterHelpers2');
        var service = newsletterHelpers2.getServiceDel('get.list');
        service.URL = service.URL + '/' + id;
        var response = service.call();
        
        var jsonResponse = {};
        if(response.ok) {
            jsonResponse = {
                Id: id,
                response: "Deleted with success",
                success: true,
            }
        }
        else {
            jsonResponse = {
                Id: id,
                response: "there was an error ! " + response.msg,
                success: false
            }
        }
        res.json(jsonResponse);
        next();
    }
);

server.get(
    'Edit',
    server.middleware.https,
    function (req, res, next) {
        //var actionUrl = dw.web.URLUtils.url('Newsletter-Handler');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-update'),
            newsletterForm: newsletterForm,
            Id: req.querystring.ClientUp
        });

        next();
    }
);

server.post(
    'update',
    server.middleware.https,
    function (req, res, next) {

        var id = req.form.ClientId;
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers2');
        var newsletterForm = server.forms.getForm('newsletter');
        var service = newsletterHelpers.getServiceUp('crm.newsletter.subscribe');
        service.URL = service.URL + '/' + id;

        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;
        var email = newsletterForm.email.htmlValue;

        var reqObject = {
            properties: {
                firstname: firstname,
                lastname: lastname,
                email: email
            }
        };
        var response = service.call(reqObject);
        var jsonResponse = {};


        if (response.ok) {
            
            jsonResponse = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                response: response.msg,
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
        }
        else {

            jsonResponse = {
                response: "there was an error ! " + response.msg,
                success: false
            };
        };
        res.json(jsonResponse);
        next();
    }
);

server.post(
    'Subscribe',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletterForm = server.forms.getForm('newsletter');
        var service = newsletterHelpers.getService('crm.newsletter.subscribe');

        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;
        var email = newsletterForm.email.htmlValue;

        var reqObject = {
            properties: {
                firstname: firstname,
                lastname: lastname,
                email: email
            }
        };
        var response = service.call(reqObject);
        var jsonResponse = {};


        if (response.ok) {
            
            jsonResponse = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                response: response.msg,
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
            var toStamp = firstname + ' ' +lastname+ ' '+ email + " added to service by crm.newsletter.subscribe"
            customLog.warn(toStamp);
        }
        else {

            var customCreated ={};
            customCreated = dw.system.HookMgr.callHook("dw.custom","create",reqObject);
            
            jsonResponse = {
                response: "there was an error ! " + response.msg + " sent to Custom Object (Newsletter) id =" + customCreated.timeStamp,
                success: false
            };
            var toStamp = response.msg + " sent to Custom Object (Newsletter) id =" + customCreated.timeStamp
            customLog.warn(toStamp);
        };
        res.json(jsonResponse);
        next();
    }
);

server.post(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')

        });
        next();
    }
);

module.exports = server.exports();