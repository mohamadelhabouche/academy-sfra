'use strict'

var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var SeekableIterator = require('dw/util/SeekableIterator');
var Logger = require('dw/system/Logger');

function jobHub() {

    var args = arguments[0];

    var serviceName = args.TestParam;
    var customObject = args.TestParam2;

    var HookMgr = require('dw/system/HookMgr');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var customObjects = CustomObjectMgr.getAllCustomObjects(customObject);
    var service = newsletterHelpers.getService(serviceName);

    if (customObjects != null) {


        while (customObjects.hasNext()) {
            var objects = customObjects.next();
            var name = objects.custom.nome;
            var surname = objects.custom.cognome;
            var email = objects.custom.email;
            var id = objects.custom.timestamp;

            var reqObject = {
                properties: {
                    firstname: name,
                    lastname: surname,
                    email: email
                }
            };

            var data = {
                customObject: customObject,
                id: id
            };

            var response = service.call(reqObject);

            if (!response.ok) {
                var customDeleted = {};
                customDeleted = dw.system.HookMgr.callHook("dw.custom2", "deleted", data);
                var toStamp = firstname + ' ' + lastname + ' ' + email + "existe already"
                var customLog = Logger.getLogger('crm service logger', 'warn');
                customLog.warn(toStamp);
            }
            else {
                var customDeleted = {};
                customDeleted = dw.system.HookMgr.callHook("dw.custom2", "deleted", data);
                var toStamp = firstname + ' ' + lastname + ' ' + email + "added"
            }

        }

        return new Status(Status.OK)
    }
    else {
        return new Status(Status.ERROR)
    }
}
exports.JobHub = jobHub