'use strict'

var Status = require('dw/system/Status');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var SeekableIterator = require('dw/util/SeekableIterator');
var Logger = require('dw/system/Logger');
/*
function jobTest() {

    var args = arguments[0];
    var customObject = {};

    customObject.name = args.TestParam;
    customObject.key = args.TestParam2;

    var objectDel = CustomObjectMgr.getCustomObject(customObject.name,customObject.key);

    if (objectDel != null) {
        Transaction.wrap(function(){
        CustomObjectMgr.remove(objectDel);
        });
        return new Status(Status.OK)
    }
    else {
        return new Status(Status.ERROR)
    }
}

*/

function jobTest() {

    var args = arguments[0];
    var customObject = {};

    customObject.type = args.TestParam;
    customObject.key = args.TestParam2;

    var customObjects = CustomObjectMgr.getAllCustomObjects(customObject.type);

    if (customObjects != null) {
        var list = [];
        var toStamp;

        while(customObjects.hasNext())
        {
            var objects = customObjects.next();
            var name = objects.custom.nome;
            var surname = objects.custom.cognome;
            var email = objects.custom.email;
            var id = objects.custom.timestamp;

            var toStamp = name + ' ' +surname+ ' '+ email + ' ' + id
            var customLog = Logger.getLogger('stamp','warn');
            customLog.warn(toStamp);
            /*
            var jasonObject = {
                "name" : name,
                "surname" : surname,
                "email" : email,
                "id" : id
            }
            list.push(jasonObject);
            */
        }
    /*
        var customLog = Logger.getLogger('stamp','warn');
        customLog.warn(list);
        */
        return new Status(Status.OK)
    }
    else {
        return new Status(Status.ERROR)
    }
}
exports.JobTest = jobTest